<?php
session_start();
if (isset($_SESSION['groupe']) && $_SESSION['groupe'] == 'admin'){
  include 'install.php';
  include 'database_fct.php';
  include 'liste_produits.php';
  include 'liste_user.php';
  ?>

  <p style="color:red;">Ajout Produits<p/>
    <?php
  if (isset($_POST['submit_prod']) && $_POST['submit_prod'] == 'Nouveau Produit'){
    if (ft_add_product($_POST['name'], $_POST['prix'], $_POST['type'], $bdd))
      echo "Produit ajouté";
    else
      echo "Echec creation d'article";
    ?><br><?php
  }
  ?>
  <form action="admin_gestion.php" method="post">
    <input type="text" name="name" value="">
    <input type="number" name="prix" value="">
    <select class="" name="type">
      <option value="pair">pair</option>
      <option value="each">each</option>
      <option value="unknown">unknown</option>
    </select>
    <input type="submit" name="submit_prod" value="Nouveau Produit">
  </form>

  <p style="color:red;">Supression Produits<p/>
  <?php
  if (isset($_POST['delete_product']) && $_POST['delete_product'] == 'Supprimer'){
      if (ft_delete_product($_POST['produit'], $bdd))
        echo "Produit Supprimé !";
      else
        echo "Erreur Suppr";
  }
  ?>
  <form class="" action="admin_gestion.php" method="post">
  <select class="" name="produit">
    <?php foreach ($list_produit as $key => $value): ?>
      <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
    <?php endforeach; ?>
  </select>
  <input type="submit" name="delete_product" value="Supprimer">
  </form>
  <br>

  <p style="color:red;">Supression User<p/>
    <?php
    if (isset($_POST['delete_user']) && $_POST['delete_user'] == 'Supprimer'){
        if (ft_delete_user($_POST['user'], $bdd))
          echo "Utilisateur Supprimé !";
        else
          echo "Erreur User Suppr";
    }
    ?>
    <form class="" action="admin_gestion.php" method="post">
    <select class="" name="user">
      <?php foreach ($list_user as $key => $value): ?>
        <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
      <?php endforeach; ?>
    </select>
    <input type="submit" name="delete_user" value="Supprimer">
    </form>
    <br>
    <p>Liste des produits : </p>
  <?php foreach ($list_produit as $key => $value){
      ?><ul><?php echo $key;?></ul><?php
    }
    ?>
  <br><br><br>
  <p>Liste des Utilisateurs : </p>
  <?php foreach ($list_user as $key => $value){
    ?><ul><?php
    echo $key;

    ?></ul><?php
    }
    ?>
  <?php
}
?>
